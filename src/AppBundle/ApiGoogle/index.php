<?php


/************************************************
 * Do zobaczenia przykładu API lokalnie, uruchom następującą komendę z poziomu folderu AppBundle:
 * php -S localhost:8080 -t ApiGoogle/
 * Oraz wyszukaj  "localhost:8080" w przeglądarce
 ************************************************/

include_once __DIR__ . '/../../../vendor/autoload.php';
include_once "base.php";

echo pageHeader("API Google - powiązane pliki z kontem i polubione filmy z youtube");


if (!$oauth_credentials = getOAuthCredentialsFile()) {
  echo missingOAuth2CredentialsWarning();
  return;
}


$redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

$client = new Google_Client();
$client->setAuthConfig($oauth_credentials);
$client->setRedirectUri($redirect_uri);
$client->addScope("https://www.googleapis.com/auth/drive");
$client->addScope("https://www.googleapis.com/auth/youtube");


if (isset($_REQUEST['logout'])) {
  unset($_SESSION['multi-api-token']);
}


if (isset($_GET['code'])) {
  $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  $client->setAccessToken($token);


  $_SESSION['multi-api-token'] = $token;


  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}


if (!empty($_SESSION['multi-api-token'])) {
  $client->setAccessToken($_SESSION['multi-api-token']);
  if ($client->isAccessTokenExpired()) {
    unset($_SESSION['multi-api-token']);
  }
} else {
  $authUrl = $client->createAuthUrl();
}


$yt_service = new Google_Service_YouTube($client);
$dr_service = new Google_Service_Drive($client);


if ($client->getAccessToken()) {
  $_SESSION['multi-api-token'] = $client->getAccessToken();

  $dr_results = $dr_service->files->listFiles(array('pageSize' => 10));

  $yt_channels = $yt_service->channels->listChannels('contentDetails', array("mine" => true));
  $likePlaylist = $yt_channels[0]->contentDetails->relatedPlaylists->likes;
  $yt_results = $yt_service->playlistItems->listPlaylistItems(
      "snippet",
      array("playlistId" => $likePlaylist)
  );
}
?>

<div class="box">
  <div class="request">
<?php if (isset($authUrl)): ?>
  <a class="login" href="<?= $authUrl ?>">Połącz mnie!</a>
<?php else: ?>
  <h3>Wyniki listy plików dla konta google:</h3>
  <?php foreach ($dr_results as $item): ?>
    <?= $item->name ?><br />
  <?php endforeach ?>

  <h3>Wyniki polubień filmów Youtube:</h3>
  <?php foreach ($yt_results as $item): ?>
    <?= $item['snippet']['title'] ?><br />
  <?php endforeach ?>
<?php endif ?>
  </div>
</div>


