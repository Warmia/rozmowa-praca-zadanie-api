<?php

function isWebRequest()
{
  return isset($_SERVER['HTTP_USER_AGENT']);
}

function pageHeader($title)
{
  $ret = "<!doctype html>
  <html>
  <head>
    <title>" . $title . "</title>
    <link href='style.css' rel='stylesheet' type='text/css' />
  </head>
  <body>\n";
  if ($_SERVER['PHP_SELF'] != "/index.php") {
    $ret .= "<p><a href='../ApiGoogle/index.php'>Powrót</a></p>";
  }
  $ret .= "<header><h1>" . $title . "</h1></header>";

  if (!headers_sent()) {
    session_start();
  }

  return $ret;
}



function missingApiKeyWarning()
{
  $ret = "
    <h3 class='warn'>
     Potrzebujesz ustawić klucz dostępu do API z 
      <a href='http://developers.google.com/console'>Google API </a>
    </h3>";

  return $ret;
}

function missingClientSecretsWarning()
{
  $ret = "
    <h3 class='warn'>
       Potrzebujesz ustawić Client ID, Client Secret i przekierowanie URI z 
      <a href='http://developers.google.com/console'>Google API </a>
    </h3>";

  return $ret;
}

function missingServiceAccountDetailsWarning()
{
  $ret = "
    <h3 class='warn'>
      Potrzebujesz pobrać swoje dane logowania w formacie JSON  do serwisu z  
      <a href='http://developers.google.com/console'>Google API</a>.
    </h3>
    <p>
    Raz pobrane przenieś je do głownego folderu repozytorium i zmień nazwę na service-account-credentials.json.
    </p>
    <p>
      W twojej aplikacji, powinienieś ustawić  zmienną środowiskową  GOOGLE_APPLICATION_CREDENTIALS.
    </p>";

  return $ret;
}

function missingOAuth2CredentialsWarning()
{
  $ret = "
    <h3 class='warn'>
      Potrzebujesz ustawić lokalizacje  danych logowania  do  klienta OAuth2 z  
      <a href='http://developers.google.com/console'>Google API </a>.
    </h3>
    <p>
     Raz pobrane przenieś je do głownego folderu repozytorium i zmień nazwę na oauth-credentials.json.
    </p>";

  return $ret;
}

function checkServiceAccountCredentialsFile()
{
  $application_creds = __DIR__ . '/../../service-account-credentials.json';

  return file_exists($application_creds) ? $application_creds : false;
}

function getOAuthCredentialsFile()
{
  $oauth_creds ='oauth-credentials.json';

  if (file_exists($oauth_creds)) {
    return $oauth_creds;
  }

  return false;
}

function setClientCredentialsFile($apiKey)
{
  $file = __DIR__ . '/../../tests/.apiKey';
  file_put_contents($file, $apiKey);
}


function getApiKey()
{
  $file = __DIR__ . '/../../tests/.apiKey';
  if (file_exists($file)) {
    return file_get_contents($file);
  }
}

function setApiKey($apiKey)
{
  $file = __DIR__ . '/../../tests/.apiKey';
  file_put_contents($file, $apiKey);
}
